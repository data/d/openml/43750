# OpenML dataset: Glass-Classification

https://www.openml.org/d/43750

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This is a Glass Identification Data Set from UCI. It contains 10 attributes including id. The response is glass type(discrete 7 values)
Content
Attribute Information:

Id number: 1 to 214 (removed from CSV file)
RI: refractive index 
Na: Sodium (unit measurement: weight percent in corresponding oxide, as are attributes 4-10) 
Mg: Magnesium 
Al: Aluminum 
Si: Silicon 
K: Potassium 
Ca: Calcium 
Ba: Barium 
Fe: Iron 
Type of glass: (class attribute) 
-- 1 buildingwindowsfloatprocessed 
-- 2 buildingwindowsnonfloatprocessed 
-- 3 vehiclewindowsfloatprocessed 
-- 4 vehiclewindowsnonfloatprocessed (none in this database) 
-- 5 containers 
-- 6 tableware 
-- 7 headlamps

Acknowledgements
https://archive.ics.uci.edu/ml/datasets/Glass+Identification
Source:
Creator: 
B. German 
Central Research Establishment 
Home Office Forensic Science Service 
Aldermaston, Reading, Berkshire RG7 4PN 
Donor: 
Vina Spiehler, Ph.D., DABFT 
Diagnostic Products Corporation 
(213) 776-0180 (ext 3014)
Inspiration
Data exploration of this dataset reveals two important characteristics :
1) The variables are highly corelated with each other including the response variables:
So which kind of ML algorithm is most suitable for this dataset Random Forest , KNN or other? Also since dataset is too small is there any chance of applying PCA or it should be completely avoided?
2) Highly Skewed Data:
Is scaling sufficient or are there any other techniques which should be applied to normalize data? Like BOX-COX Power transformation?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43750) of an [OpenML dataset](https://www.openml.org/d/43750). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43750/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43750/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43750/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

